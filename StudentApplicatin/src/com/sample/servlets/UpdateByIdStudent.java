package com.sample.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sample.service.StudentServiceImpl;

/**
 * Servlet implementation class UpdateByIdStudent
 */
@WebServlet("/UpdateByIdStudent")
public class UpdateByIdStudent extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//getting session object updateStudent servlet
		HttpSession ses = request.getSession(true);
		Object obj = ses.getAttribute("id");
		
		PrintWriter out = response.getWriter();
		int id = (Integer) obj;
		
		//getting values from html form
		String name = request.getParameter("uname");
		int age = Integer.parseInt(request.getParameter("uage"));
		
		//calling update student method
		boolean result = new StudentServiceImpl().UpdateById(id,age,name);
		if(result) {
			out.print("<html><body>");
			out.print("Updated Successfully");
			out.print("<hr><a href='index.html'>Back to Home</a>");
			out.print("</body></html>");
		}	
	}
}
