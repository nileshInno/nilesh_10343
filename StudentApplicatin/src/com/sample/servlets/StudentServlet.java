package com.sample.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sample.bean.Student;
import com.sample.service.StudentService;
import com.sample.service.StudentServiceImpl;

/**
 * Servlet implementation class StudentServlet
 */
@WebServlet("/StudentServlet")
public class StudentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
  
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// getting values from html form
		int studentId = 0;
		String studentName = null;
		int studentAge = 0;
		PrintWriter out = response.getWriter();
		try {
			studentId = Integer.parseInt(request.getParameter("id"));
			studentName = request.getParameter("name");
			studentAge = Integer.parseInt(request.getParameter("age"));
		}catch(Exception e) {
			out.print("<html><body>");
			out.print("<label style='color: red;'>Please Enter Student Id</label>");
			out.print("</body></html>");
			//if user did not entered id then asking again
			RequestDispatcher rd = request.getRequestDispatcher("insert.html");
			rd.include(request, response);
		}
		//creating student object
		Student student = new Student();
		student.setId(studentId);
		student.setName(studentName);
        student.setAge(studentAge);
		
        //passing student object to insertStudent method
		StudentService service = new StudentServiceImpl();
		boolean result = false;
		if(studentId != 0 && studentName != null && studentAge != 0) {
			result = service.insertStudent(student);
		}
		//result = service.insertStudent(student);
		
		out.print("<html><body>");
		if(result) {
			out.print("<h2>Student Saved</h2>");
			out.print("<hr><a href='index.html'>Back to Home</a>");
		}
		else {
			out.print("<h2>Something wrong</h2>");
			out.print("<hr><a href='index.html'>Back to Home</a>");
		}
		
		out.print("</body></html>");
		out.close();
		
		
	}

}
