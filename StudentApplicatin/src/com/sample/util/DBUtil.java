package com.sample.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class DBUtil {
	/**
	 * This method Establish connection with database
	 * @return connection object
	 */
	public static Connection getCon() {
		Connection con = null;
		
		try {
			//loading the driver
			Class.forName("com.mysql.cj.jdbc.Driver");
			//Establish the connection
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/studentdb", "root", "innominds");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		//returning connection object
		return con;
	}
	
	
	
}
