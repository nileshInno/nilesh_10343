package com.sample.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.sample.bean.Address;
import com.sample.bean.Student;
import com.sample.util.DBUtil;


public class StudentDAOImpl implements StudentDAO {

	/**
	 * This method insert New student record into database
	 * @param student object
	 * @return boolean result
	 */
	public boolean createStudent(Student student) {		
		Connection con = null;
		PreparedStatement ps = null;
		boolean result = false;
		
		//Preparing sql query to insert new student record
		String sql = "insert into student_tbl values( ?, ?, ?, ?, ?)";
		try {
			
			//Establishing connection with database
			con = DBUtil.getCon();
			ps = con.prepareStatement(sql);
			
			//setting parameter to sql query
			ps.setInt(1, student.getId());
			ps.setString(2, student.getName());
			ps.setInt(3, student.getAge());
			ps.setString(4, student.getAddress().getCity());
			ps.setString(5, student.getAddress().getState());
			//executing sql query
			int rowsEffected = ps.executeUpdate();
			if(rowsEffected == 1) {
				result = true;
			} 
		}
		catch (SQLException e){
			e.printStackTrace();
		}
		finally {
			try {
				//closing connection with database
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		//returns true if data inserted successfully otherwise return false
		return result;
	}

	/**
	 * This Method search student in database as per given id
	 * @param student id
	 * @return student object
	 */
	public Student searchById(int id) {
		
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Address address = null;
		Student st = null;
		
		//Preparing sql query to insert new student record
		String sql = "select * from student_tbl where id = ?";
		try {
			//Establishing connection with database
			con = DBUtil.getCon();
			ps = con.prepareStatement(sql);
			
			//setting parameter to sql query
			ps.setInt(1, id);
			
			//executing sql query
			rs = ps.executeQuery();
			while(rs.next()) {
				address = new Address();
				address.setCity(rs.getString(4));
				address.setState(rs.getString(4));
				//setting values to student object
				st= new Student();
				st.setId(rs.getInt(1));
				st.setName(rs.getString(2));
				st.setAge(rs.getInt(3));
				st.setAddress(address);
			}
		}
		catch (SQLException e){
			e.printStackTrace();
		}
		finally {
			try {
				//closing connection with database
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		//returning student object which contains query result values
		return st;
	}

	/**
	 * This method get all student present in database
	 * @return list object
	 */
	public List<Student> getAllStudents() {
		
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		Student st = null;
		Address address = null;
		//creating list to hold student data
		List <Student> studentList = new ArrayList<Student>();
		
		//Preparing sql query to search student record
		String sql = "select * from student_tbl ";
		try {
			//Establishing connection with database
			con = DBUtil.getCon();
			stmt= con.createStatement();
			
			//executing sql query
			rs = stmt.executeQuery(sql);
			while(rs.next()) {
				address = new Address();
				address.setCity(rs.getString(4));
				address.setState(rs.getString(4));
				//setting values to student object
				st= new Student();
				st.setId(rs.getInt(1));
				st.setName(rs.getString(2));
				st.setAge(rs.getInt(3));
				st.setAddress(address);
				//adding student object into list
				studentList.add(st);
			}
		}
		catch (SQLException e){
			
		}
		finally {
			try {
				//closing connection with database
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		//returning student list which holding students
		return studentList;
	}
	
	/**
	 * Thsi Method update student age and name as per given id
	 * @param id
	 * @param age
	 * @param name
	 * @return
	 */
	public boolean UpdateById(int id , int age, String name) {
		boolean result = false;
		Connection con = null;
		PreparedStatement ps = null;
		
		//Preparing sql query to update student record
		String sql = "update student_tbl set name = ? , age = ?  where id = ? ";
		try {
			//Establishing connection with database
			con = DBUtil.getCon();
			ps = con.prepareStatement(sql);
			
			//setting parameter to query
			ps.setString(1, name);
			ps.setInt(2, age);
			ps.setInt(3, id);
			
			//executing sql query
			int i = ps.executeUpdate();
			if(i == 1) {
				result = true;
			}
		}
		catch (SQLException e){
			e.printStackTrace();
		}
		finally {
			try {
				//closing connection with database
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		//returning boolean result
		return result;
	}
	
	/**
	 * This method delete student record as per given id
	 * @param id
	 * @return
	 */
	public boolean DeleteById(int id) {
		boolean result = false;
		Connection con = null;
		PreparedStatement ps = null;
		
		//Preparing sql query to delete student record
		String sql = "delete from student_tbl where id = ? ";
		try {
			//Establishing connection with database
			con = DBUtil.getCon();
			ps = con.prepareStatement(sql);
			
			//setting parameter to query
			ps.setInt(1, id);
			
			//executing sql query
			int i = ps.executeUpdate();
			if(i == 1) {
				result = true;
			}
		}
		catch (SQLException e){
			e.printStackTrace();
		}
		finally {
			try {
				//closing connection with database
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		//returning boolean result
		return result;
	}

}











