package com.sample.bean;

/**
 * Student Bean Class
 * @author Nilesh
 */

public class Student {

	private int id;
	private  String name;
	private int age;
	private Address address;

	/**
	 * Get the id from student object
	 * @return
	 */
	public int getId() {
		return id;
	}
	
	
	public Address getAddress() {
		return address;
	}


	public void setAddress(Address address) {
		this.address = address;
	}


	/**
	 * Set Id to student id
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * Get the name from student object
	 * @return
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * set the name to student object
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * get the age from student object
	 * @return
	 */
	public int getAge() {
		return age;
	}
	
	/**
	 * set age to student object
	 * @param age
	 */
	public void setAge(int age) {
		this.age = age;
	}
	
	
	
}
