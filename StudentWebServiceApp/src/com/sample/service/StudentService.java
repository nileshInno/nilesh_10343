package com.sample.service;

import java.util.List;

import com.sample.bean.Student;

public interface StudentService {
	
	boolean insertStudent( Student student );
	
	Student findById(int id);
	
	List<Student> fetchAllStudents();
	
}
