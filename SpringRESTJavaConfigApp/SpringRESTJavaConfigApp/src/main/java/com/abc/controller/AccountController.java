package com.abc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.abc.entities.Account;
import com.abc.service.AccountService;

@RestController
public class AccountController {

	@Autowired
	private AccountService accService;

	/**
	 * This resource method search account by account number
	 * @param accNo
	 * @return Account
	 */
	@GetMapping("/account/search/{id}")
	public ResponseEntity<Account> searchByAccount(@PathVariable("id") int accNo) {
		
		//searching whether account present is not is not returning fail
		Account account = accService.searchByAccNo(accNo);
		if (account == null) {
			return new ResponseEntity<Account>(HttpStatus.NOT_FOUND);
		}
		//if prsent returning account details
		return new ResponseEntity<Account>(account, HttpStatus.OK);
	}

	@PostMapping("/account/create")
	public ResponseEntity<String> createAccount(@RequestBody Account account) {
		
		//inserting new account 
		boolean result = accService.insertAccount(account);
		if (result) {
			return new ResponseEntity<String>("New account is created", HttpStatus.OK);
		} else {
			return new ResponseEntity<String>("Something went wrong", HttpStatus.NOT_FOUND);
		}
	}

	@DeleteMapping("/account/delete/{id}")
	public ResponseEntity<String> deleteAccount(@PathVariable("id") int accNo) {
		Account account = accService.searchByAccNo(accNo);
		if (account == null) {
			return new ResponseEntity<String>("account not found", HttpStatus.NOT_FOUND);
		} else {
			accService.deleteAccount(accNo);
			return new ResponseEntity<String>("account deleted", HttpStatus.OK);
		}
	}

	@GetMapping("/account/getall")
	public ResponseEntity<List<Account>> displayAllAccount() {
		List<Account> list = accService.getAllAccounts();
		if (list.isEmpty()) {
			return new ResponseEntity<List<Account>>(HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<List<Account>>(list, HttpStatus.OK);
		}
	}

	@GetMapping("/account/deposite")
	public String deposite(@RequestParam("accno") int accNo, @RequestParam("amount") double amount) {
		String message = accService.deposite(accNo, amount);
		return message;
	}

	@GetMapping("/account/withdraw")
	public String withdraw(@RequestParam("accno") int accNo, @RequestParam("amount") double amount) {
		String message = accService.withdraw(accNo, amount);
		return message;
	}

	@PutMapping("/account/update")
	public ResponseEntity<String> update(@RequestBody Account account) {	
		
		Account account1 = accService.searchByAccNo(account.getAccno());
		if (account1 == null) {
			return new ResponseEntity<String>("account not found", HttpStatus.NOT_FOUND);
		} else {
			accService.updateAccount(account);
			return new ResponseEntity<String>("account updated", HttpStatus.OK);
		}
	}
}
