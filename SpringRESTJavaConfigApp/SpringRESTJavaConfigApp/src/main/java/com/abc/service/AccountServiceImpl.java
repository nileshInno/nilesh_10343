package com.abc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;

import com.abc.dao.AccountDao;
import com.abc.entities.Account;

@Service
public class AccountServiceImpl implements AccountService{

	@Autowired
	private AccountDao accountDao;
	
	@Transactional
	@Override
	public Account searchByAccNo(int accNo) {
		
		return accountDao.getByAccNo(accNo);
	}

	@Transactional
	@Override
	public boolean insertAccount(Account account) {
		boolean result = accountDao.insertAccount(account);
		return result;
	}

	@Transactional
	@Override
	public boolean deleteAccount(int accNo) {
		// TODO Auto-generated method stub
		
		return accountDao.deleteAccount(accNo);
	}

	@Transactional
	@Override
	public List<Account> getAllAccounts() {
		List<Account> list = accountDao.getAllAccounts();
		return list;
	}

	@Transactional
	@Override
	public String withdraw(int accNo, double amount) {
		String message = accountDao.withdraw(accNo, amount);
		return message;
	}

	@Transactional
	@Override
	public String deposite(int accNo, double amount) {
		String message = accountDao.deposite(accNo, amount);
		return message;
	}
	@Transactional
	@Override
	public void updateAccount(Account account) {
		accountDao.updateAccount(account);
	}

}
