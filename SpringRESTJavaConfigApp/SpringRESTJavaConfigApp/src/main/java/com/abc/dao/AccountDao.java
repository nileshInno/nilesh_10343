package com.abc.dao;

import java.util.List;

import com.abc.entities.Account;

public interface AccountDao {
	
	 Account getByAccNo(int accNo);
	 boolean insertAccount(Account account);
	 boolean deleteAccount(int accNo);
	 List<Account> getAllAccounts();
	 String withdraw(int accNo,double amount);
	 String deposite(int accNo,double amount);
	 void updateAccount(Account account);
}
