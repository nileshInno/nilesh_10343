package com.abc.collectionEnjection;

import java.util.Iterator;
import java.util.List;

public class MyExam {
	
	private List<Question> list;

	public MyExam(List<Question> list) {
		super();
		this.list = list;
	}

	public void displayQuestions() {
//		Iterator itr = list.iterator();
		for(Question temp : list) {
			temp.display();
		}
	}

}
