package com.abc.collectionEnjection;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class QuestionMain {

	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("classpath:com/abc/resources/question.xml");
		MyExam que1 = (MyExam) context.getBean("myExam");
		
		que1.displayQuestions();
	}

}
