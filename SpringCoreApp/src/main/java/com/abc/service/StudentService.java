package com.abc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.abc.bean.Student;
import com.abc.dao.StudentData;
@Service
public class StudentService {
	@Autowired
	private StudentData studData;
	
	public StudentData getStudData() {
		return studData;
	}

	public void setStudData(StudentData studData) {
		this.studData = studData;
	}
	
	/**
	 * This Method Insert new student in db
	 * @param student
	 * @return
	 */
	public boolean insertStudent(Student student) {
		boolean result = studData.saveStudent(student);
		return result;	
	}
	
	/**
	 * get student by ID
	 * @param id
	 * @return Student Object
	 */
	public Student fetchStudentById(int id) {
		Student std = studData.getStudentById(id);
		return std;	
	}
	
	
	/**
	 * Return all student present in database
	 * @return list of student
	 */
	public List<Student> getAllStudent(){
		List<Student> list = studData.getAllStudent();
		return list;	
	}
}
