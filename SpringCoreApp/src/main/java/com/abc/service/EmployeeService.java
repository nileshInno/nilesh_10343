package com.abc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.abc.bean.Employee;
import com.abc.dao.EmployeeData;

@Service
 public class EmployeeService {
	 
	@Autowired
	 EmployeeData empData;
	 
	public EmployeeData getEmpData() {
		return empData;
	}

	public void setEmpData(EmployeeData empData) {
		this.empData = empData;
	}

	public void setEmployee(Employee emp) {
		empData.saveEmployee(emp);
	}
}
