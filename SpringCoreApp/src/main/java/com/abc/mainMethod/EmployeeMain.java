package com.abc.mainMethod;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import com.abc.bean.Employee;
import com.abc.controller.EmployeeController;
import com.abc.javaConfiguration.JavaConfiguration;

public class EmployeeMain {
	public static void main(String[] args) {
		
		ApplicationContext context = new AnnotationConfigApplicationContext(JavaConfiguration.class);

		EmployeeController empCon = (EmployeeController) context.getBean(EmployeeController.class);
		
		Employee emp = (Employee) context.getBean(Employee.class);
		
		emp.setEmpName("sdfhskfjf");
		
		empCon.setEmployee(emp);
	}
}
