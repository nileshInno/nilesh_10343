package com.examtracking.provider;

import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.examtracking.bean.SetExamTimeTable;
import com.examtracking.bean.SetMarkSheet;
import com.examtracking.service.ExamDetailsImpl;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

@Path("/examination")
public class ExamProvider {

	/**
	 * This is resource method which is used to add new Time table in database
	 * 
	 * @param time table
	 * @return
	 */
	@POST
	@Path("/setexamtimetable")
	@Produces("application/json")
	@Consumes("text/plain")
	public String setTimeTable(String timetableList) {
		String str = null;
		Gson gson = new Gson();
		SetExamTimeTable timeTableData = gson.fromJson(timetableList, SetExamTimeTable.class);
		
		//For testing purpose hardcoded values
		timeTableData.setBranchId(1);
		timeTableData.setExamTypeId(2);
		
		List<SetExamTimeTable> list = new ArrayList<SetExamTimeTable>();
		list.add(timeTableData);
		boolean result = new ExamDetailsImpl().SetExamTimeTable(list);
		if (result) {
			str = "time_table_created";
		}
		return str;
	}

	/**
	 * This is resource method which is return time table from database
	 * 
	 * @param time table
	 * @return
	 */
	@GET
	@Path("/getexamtimetable")
	@Produces("application/json")
	@Consumes("text/plain")
	public String getTimeTable(@QueryParam("branchId") String branchId, @QueryParam("examChoice") String examChoice) {
		int branch_id = 0;
		int exam_choice = 0;
		try {
			branch_id = Integer.parseInt(branchId);
			exam_choice = Integer.parseInt(examChoice);
		} catch (NumberFormatException e) { // handle your exception
			e.printStackTrace();
		}
		Gson gson = new Gson();
		List<SetExamTimeTable> list = new ExamDetailsImpl().getExamTimeTable(branch_id, exam_choice);
		String studentJsonString = gson.toJson(list);
		
		return studentJsonString;
	}
	/**
	 * This is resource method which is return time table from database
	 * 
	 * @param time table
	 * @return
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getAllexamtimetable")
	public String getTimeTableOfAllExam() {
		
		Gson gson = new Gson();
		List<SetExamTimeTable> list = new ExamDetailsImpl().getAllExamTimeTable();
		String studentJsonString = gson.toJson(list);		
		return studentJsonString;
	}
	/**
	 * This is resource method checks whether marks sheet already present or not 
	 * @param time table
	 * @return
	 */
	@GET
	@Path("/checkmarkssheet")
	@Produces("application/json")
	@Consumes("text/plain")
	public String checkMarksSheet(@QueryParam("student_id") String student_id, @QueryParam("examChoice") String examChoice) {
		int stud_id = Integer.parseInt(student_id);
		int exam_choice = Integer.parseInt(examChoice);
		boolean result = new ExamDetailsImpl().checkMarks(stud_id, exam_choice);
		if(result) {
			return "Already present";
		}else {
			return "Not present, you can set";
		}	
	}
	/**
	 * This is resource method checks whether marks sheet already present or not 
	 * @param time table
	 * @return
	 */
	@GET
	@Path("/setmarkssheet")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String setMarksSheet(SetMarkSheet marks) {
		
		int exam_choice = marks.getExam_id();
		
		int student_id = (int) marks.getStudent_id();
		
		new ExamDetailsImpl().setMarkSheetForStudent(marks,student_id,exam_choice);
		boolean result = false;
		if(result) {
			return "Mark sheet created";
		}else {
			return "Something went wrong";
		}	
	}
}
