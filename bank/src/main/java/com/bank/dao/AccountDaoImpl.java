package com.bank.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.bank.entities.Account;

public interface AccountDaoImpl extends JpaRepository<Account, Integer>{
	
	
//	@Query("from Account where accno =:accNo")
	public Account findByAccno(String accNo);
}
