package com.bank.service;

import java.util.List;

import com.bank.entities.Account;


public interface AccountService {
	
	Account searchByAccNo(String accNo);
	Account insertAccount(Account account);
    boolean deleteAccount(int accNo);
    List<Account> getAllAccounts();
//    String withdraw(int accNo,double amount);
//	String deposite(int accNo,double amount);
//	void updateAccount(Account account);
}
