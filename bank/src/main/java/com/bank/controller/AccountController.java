package com.bank.controller;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bank.entities.Account;
import com.bank.service.AccountService;

@RestController
@RequestMapping("/bank")
public class AccountController {

	@Autowired
	private AccountService account;
	
	@PostMapping("/create")	
	public ResponseEntity<?> createAccount(@RequestBody Account newAcc) {
		Account acc = account.insertAccount(newAcc);
		if(acc != null) {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Account Created\",\"statuscode\":" + 200 + ",\"Account Number is\":"+acc.getAccno()+"}");
		}else {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Not Created\",\"statuscode\":" + 201 +"}");
		}
	}
	
	@GetMapping("/get/{accno}")	
	public ResponseEntity<?> searchAccount(@PathParam("accno") String accNo) {
		Account acc = account.searchByAccNo("1001");
		if(acc != null) {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Account Created\",\"statuscode\":" + 200 + ",\"Account Number is\":"+acc+"}");
		}else {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"No Account Found\",\"statuscode\":" + 201 +"}");
		}
	}
	
	@PutMapping("/delete")	
	public ResponseEntity<?> deleteAccount(@PathParam("accno") String accNo) {
		boolean acc = account.deleteAccount(1001);
		if(acc) {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Account deleted\",\"statuscode\":" + 200 + ",\"Account Number is\":"+acc+"}");
		}else {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"No Account Found\",\"statuscode\":" + 201 +"}");
		}
	}
}
